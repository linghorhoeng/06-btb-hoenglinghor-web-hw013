import React, { Component } from 'react';
import {Form, Button,  } from "react-bootstrap";
import Axios from "axios";

export default class Update extends Component {

    constructor() {
        super();
        this.state = {
            DATA: {},
                TITLE: "",
                DESCRIPTION: "",
                IMAGE:""
            
        };      
    }  
  changeTitleHandler=(e)=>{
      this.setState({TITLE:e.target.value})
  } 
  changeDesHandler=(e)=>{
     this.setState({DESCRIPTION:e.target.value})
 } 
    saveHandle=()=>{    
    
      
      let alldata = {
        TITLE: this.state.TITLE,
        DESCRIPTION: this.state.DESCRIPTION,
      }; 
        Axios.put( `http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`,alldata)
        .then(res =>{
          console.log(res.this.props.match.params.id);
          alert("YOU HAVE BEEN UPDATED SUCCESSFULLY");
          this.props.history.push('/')
      })          
      .catch((err) => {
          console.log(err);
        });
      } 
    
  render() {
    var view = this.props.match.params.id;
    return (
        <div className="container">
        <h1>Update Article</h1>
        <div className="row">
        <div className="col-8">
        <Form onSubmit={this.saveHandle.bind(this)}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control type="text"  onChange={this.changeTitleHandler.bind(this)}/> {this.state.DATA.TITLE}
          <span  style={{ color: "red" }}></span>
        </Form.Group>    
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Description</Form.Label>
          <Form.Control type="text"  onChange={this.changeDesHandler.bind(this)}/>{this.state.DATA.DESCRIPTION}
          <span  style={{ color: "red" }}></span>
        </Form.Group>
        <Button variant="primary" type="submit">
          Save
        </Button>
      </Form>
        </div>
        <div className="col-4">
        <Form.Group>
        <img src={"https://media.wired.com/photos/5cdefc28b2569892c06b2ae4/master/w_2560%2Cc_limit/Culture-Grumpy-Cat-487386121-2.jpg"} alt="image" width="70%"/>    
        </Form.Group>
        </div>
    </div>
  </div>
    );
  }
}
