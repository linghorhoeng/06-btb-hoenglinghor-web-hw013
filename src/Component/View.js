import React, { Component } from 'react';
import Axios from "axios";
let id;
export default class View extends Component {
constructor() {
    super();
    this.state = {
      id: "",
      data: {},
    };
  }
  componentDidMount() {
    Axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
      .then((res) => {
        this.setState({
          data: res.data.DATA,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
      
    var view = this.props.match.params.id;
    console.log(view);
    id=view;
    return (
        <div className="container">
        <h1 style={{padding:"10px",margin:"10px"}}>Article</h1> 
        <div className="row">
          <div className="col-4">
            <img src ={this.state.data.IMAGE== null ? "https://media.wired.com/photos/5cdefc28b2569892c06b2ae4/master/w_2560%2Cc_limit/Culture-Grumpy-Cat-487386121-2.jpg" : this.state.data.IMAGE} 
             alt="image" style={{width:"100%"}}/>
          </div>
          <div className="col-8">
            <h1>{this.state.data.TITLE}</h1>
            <h3 style={{paddingLeft:"10px"}} >{this.state.data.DESCRIPTION}</h3>
          </div>
        </div>
        </div>
    );
  }
}
