import React, { Component } from 'react';
import {Table } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "axios";

export default class Home extends Component {
   
  constructor() {
  super();
  this.substring = this.substring.bind(this);
  this.state = {
      data: [],
        
      };
    }
  substring = (d) => {
  let year = d.substring(0, 4);
  let month = d.substring(4, 6);
  let day = d.substring(6, 8);
  let date = day + "/" + month + "/" + year;
  return date;
  };
  deleteHandler=(e)=>{
      Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${e.target.value}`)
          .then((res) => {
            console.log(res);
            alert(res.data.MESSAGE);
            this.componentWillMount()
          })
          .catch((err) => {
            console.log(err);
          });
    }
componentWillMount() {
  Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
        .then((res) => {
        this.setState({
        data: res.data.DATA,
      });
    })
    .catch((err) => {
    console.log(err);
    });     
}  

  render() {
      var allData = this.state.data.map((allData) =>(
          <tr key ={allData.ID}>
          <td style={{width:"7%"}}>{allData.ID}</td>
          <td style={{width:"15%"}}>{allData.TITLE}</td>
          <td style={{width:"23%"}}>{allData.DESCRIPTION}</td>
          <td style={{width:"10%"}}>{this.substring(allData.CREATED_DATE)}</td>
          <td style={{width:"20%"}}><img src={allData.IMAGE!==''?allData.IMAGE :"https://vignette.wikia.nocookie.net/webarebears/images/f/fa/Panda_png.png/revision/latest?cb=20180430235229"}
           alt="image" width="50%" /></td>       
        <td style={{width:"25%"}}>
         <Link to ={`/View/${allData.ID}`}>     
        <button type="button" class="btn btn-primary" style={{marginLeft:"2%"}}>View</button>
        </Link> 
        <Link to ={`/Update/${allData.ID}`}>
        <button type="button" class="btn btn-warning" style={{marginLeft:"2%",marginRight:"1%"}}>Edit</button>  </Link>
        <button type="button" class="btn btn-danger" style={{marginRight:"2%"}} onClick={this.deleteHandler.bind(this)} value={allData.ID}>Delete</button>
      </td>
        </tr>
      ))
    return (
      <div>
      <div className="container">
      <h1 style={{textAlign:"center", padding:"20px"}}>Article management</h1>
      <center>
      <Link to="/Add">
      <button style={{marginBottom:"20px"}}  className="btn btn-dark">Add new Article</button>
      </Link>
      </center>
      <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Title</th>
          <th>Description</th>
          <th>Create Date</th>
          <th>Image</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      {allData}
      </tbody>
    </Table>
    </div>
      </div>
    );
  }
}
