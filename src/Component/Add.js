import React, { Component } from 'react';
import {Form, Button,  } from "react-bootstrap";
import Axios from "axios";

export default class Add extends Component {
  constructor() {
  super();
  this.state = {
      DATA: {},
          TITLE: "",
          DESCRIPTION: "",
          IMAGE:""    ,
          title:"",
          des:""
  };
   
}    

 changeTitleHandler=(e)=>{
     this.setState({TITLE:e.target.value})
 } 
 changeDesHandler=(e)=>{
    this.setState({DESCRIPTION:e.target.value})
} 
validate=()=>{
  if(this.state.TITLE==""&&this.state.DESCRIPTION==""){
    this.setState({
      title:"Title cannot be blank",
      des:"Description cannot be blank"
    })
    return false
  }else if(this.state.TITLE==""&&this.state.DESCRIPTION!=""){
    this.setState({
      title:"Title cannot be blank",
      des:""
    })
    return false
  }else if(this.state.TITLE!=""&&this.state.DESCRIPTION==""){
    this.setState({
      title:"",
      des:"Description cannot be blank"
    })
    return false
  }else{
    this.setState({
      title:"",
      des:""
    })
    return true
  }
}
  submitHandle=(e)=>{
    e.preventDefault()
    const isValid=this.validate();
    if(isValid==true){
    console.log(this.state)
      Axios.post('http://110.74.194.124:15011/v1/api/articles',{
        TITLE: this.state.TITLE,
        DESCRIPTION: this.state.DESCRIPTION,
        IMAGE:this.state.IMAGE
      })
      .then(res =>{
        console.log(res.data.DATA);
        alert(res.data.MESSAGE);
        this.props.history.push('/')
    })          
    .catch((err) => {
        console.log(err);
      });
    }else{
      alert("INVALID INPUT")
    }
  } 

  render() {
    return (
      <div className="container">
      <h1>Add Article</h1>
      <div className="row">
      <div className="col-8">
      <Form onSubmit={this.submitHandle.bind(this)}>
      <Form.Group controlId="formBasicEmail">
        <Form.Label>Title</Form.Label>
        <Form.Control type="text" placeholder="Enter title"  onChange={this.changeTitleHandler.bind(this)} />
        <span style={{ color: "red",fontSize:"14px" }}>{this.state.title}</span>
      </Form.Group>    
      <Form.Group controlId="formBasicPassword">
        <Form.Label>Description</Form.Label>
        <Form.Control type="text" placeholder="Enter description"  onChange={this.changeDesHandler.bind(this)}/>
        <span  style={{ color: "red",fontSize:"14px" }}>{this.state.des}</span>
      </Form.Group>
      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
      </div>
      <div className="col-4">
      <Form.Group>
      <img src={"https://vignette.wikia.nocookie.net/webarebears/images/f/fa/Panda_png.png/revision/latest?cb=20180430235229"} alt="image" width="50%"/>    
      </Form.Group>
      </div>
  </div>
</div>
    );
  }
}
