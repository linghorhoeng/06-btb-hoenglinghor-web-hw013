import './App.css';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import React, { Component } from 'react';
import Main from './Component/Main';
import Home from './Component/Home';
import View from './Component/View';
import Add from './Component/Add';
import Update from './Component/Update';
export default class App extends Component {
  render() {
    return (
      <div>
      <Router>
      <Main/>
      <Switch> 

      <Route path='/' exact component={Home}/>
      <Route path='/Home' exact component={Home}/>
      <Route path="/View/:id" component={View} />
      <Route path='/Add'  component={Add} />
      <Route path='/Update/:id' component={Update} />
      )}
    />
      </Switch>
      </Router>
      </div>
    );
  }
}
